// ARITHMETIC OPERATIONS

let x = 200;
let y = 18;

// addition

let sum = x+y;
console.log("Result of addition: " + sum);

// subtraction
let difference = x-y;
console.log("Result of subtraction operator: " + difference);

// multiplication
let product = x*y;
console.log("Result of product operator: " + product);

// division
let quotient = x/y;
console.log("Result of division operator: " + quotient);

// modulo
let modulo = x%y;
console.log("Result of modulo operator: " + modulo);

// reassigning value to a variable
assignmentNumber = 8;

assignmentNumber = assignmentNumber + 2; 
console.log("Result of assignmentNumber: " + assignmentNumber);

assignmentNumber += 2; // the same as assignmentNumber = assignmentNumber + 2
console.log("Result of assignmentNumber: " + assignmentNumber);

assignmentNumber -= 2;
console.log("Result of assignmentNumber: " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of assignmentNumber: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of assignmentNumber: " + assignmentNumber);

// PEMDAS (Order of Operations) - combinations of multiple arithmetic operators will follow the pemdas rule
/*
	Parenthesis
	Exponent
	Multiplication
	Division
	Addition
	Subtraction

	Note: will also follow left to right rule
*/
// Multiple Operators and Parenthesis

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

let pemdas = (1+(2-3)) * (4/5);
console.log(pemdas);

// INCREMENT and DECREMENT - operators that adds or subtracts a variable's value by 1 and reassign the value of the said variable

let z = 1;

// pre-increment
let increment = ++z;
console.log(increment);
console.log(z);

// post-increment
z=1;
let increment2 = z++;
console.log(increment2);
console.log(z);
increment2 = z++;
console.log(increment2);

// pre-decrement
z=1;
let decrement = --z;
console.log(decrement);
console.log(z);

// post-decrement
z=1
let decrement2 = z--;
console.log(decrement2);
console.log(z);

// TYPE COERCION - is the automatic conversion of values from one data type to another

let numA = 10;
let numB = "12";

let coercion = numA+numB;
console.log(coercion);
console.log(typeof coercion);
// when adding number and string, result is string

// number and boolean 
let numX = 24;
let numY = true;
	// boolean values are
		// true = 1;
		// false = 0;
coercion = numX + numY;
console.log(coercion);
console.log(typeof coercion);

// COMPARISON OPERATORS - used to compare values, it returns a boolean value

//Equality Operator (==);
/*
	Checks whether the operands are equal/have the same content
	Attempts to compare operands of diffent data types
	Return a boolean value

*/
let juan = "juan";
console.log(1 == 1); //true
console.log(1 == 2); //false
console.log(1 == '1'); //true
console.log(0 == false); // true
//Compares two strings that are the same
console.log('juan' == "juan"); //true
//Compares a string with the variable "juan" declared above
console.log('juan' == juan); //true

//Inequality Operator(!=)
/*
	Checks whether the operand are NOT equal/have different content

*/

console.log(1 != 1); // false
console.log(1 != 2); //true
console.log(1 != '1'); //false
console.log(0 != false); //false
console.log('juan' != 'juan'); //false
console.log('juan' != juan); // false

//Strict Equality Operator (===)
/*
	-Checks whether the operands are equal/have the same content
	-Also COMPARES the data types of 2 values

*/

console.log(1===1); //true
console.log(1===2); //false
console.log(1==="1"); //false
console.log(0===false); //false
console.log('juan'==='juan'); //true
console.log('juan'=== juan); //true

//Strict Inequality Operator
/*
	-Checks whether the operands are NOT equal/have the same content
	-Also COMPARED the data types of 2 values
	
*/

console.log(1 !== 1); //false
console.log(1 !== 2); // true
console.log(1 !== "1"); //true
console.log(0 !== false); //true
console.log('juan' !== 'juan'); //false
console.log('juan' !== juan); //false


//Relational Operators

let a = 50;
let b = 65;

//GT or Greater than operator ( > )
let isGreaterThan = a > b;
console.log(isGreaterThan); //false
//LT or Less Than operator ( < )
let isLessThan = a < b;
console.log(isLessThan); //true
//Greater than or Equal operator ( >= )
let isGTorEqual = a >= b;
console.log(isGTorEqual); //false
//Less Than or Equal operator ( <= )
let isLTorEqual = a <= b;
console.log(isLTorEqual); //true

let numStr = "30";
console.log(a > numStr); //true

let str = "twenty";
console.log(b >= str); // false


//Logical Operator

let isLegalAge = true;
let isRegistered = false;


//Logical AND Operator (&& - Double Ampersand)
//true && true = true
//true && false = false

let allRequirementsMet = isLegalAge && isRegistered;
console.log(allRequirementsMet);

//Logical OR Operator (|| - Double Pipe)
//Returns true if one of the operands are true
// true || true = true
// true || false = true
// false || false = false


let someRequirementsMet = isLegalAge || isRegistered;
console.log(someRequirementsMet); //true


//Logical NOT Operator (! - Exclamation Point)
//Return the opposite value

let someRequirementsNotMet = !isRegistered;
console.log(someRequirementsNotMet); //true